package com.binus.Exception;

import org.junit.Test;

import static org.junit.Assert.*;

public class OptionHandlerTest {

    @Test
    public void isChoosableTest() {
        OptionHandler optionHandler = new OptionHandler();
        assertEquals(6, optionHandler.isChoosable(0));
        assertEquals(3, optionHandler.isChoosable(3));
        assertEquals(6, optionHandler.isChoosable(6));
    }


}