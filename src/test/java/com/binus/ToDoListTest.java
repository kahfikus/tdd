package com.binus;

import com.binus.Repository.ToDoList;
import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void taskReturnedIsNullTest(){
        ToDoList todo = new ToDoList();
        String task = todo.getTaskById(0);
        assertNull(task);
    }
    @Test
    public void taskReturnedIsCorrectTest() {
        ToDoList todo = new ToDoList();
        String task = todo.getTaskById(1);
        assertEquals("1. Makan [DONE] Daily Activity", task);
        task = todo.getTaskById(2);
        assertEquals("2. Minum [NOT DONE] Daily Activity", task);
    }

    @Test
    public void taskListIsNotNull() {
        ToDoList todo = new ToDoList();
        assertNotNull(todo);
    }

    @Test
    public void taskListGetAllIsCorrect() {
        ToDoList todo = new ToDoList();
        assertEquals("1. Makan [DONE] Daily Activity\n2. Minum [NOT DONE] Daily Activity\n3. Bermain Sepak Bola [DONE] Olahraga\n4. Bermain Basket [NOT DONE] Olahraga", todo.getAllTask());
    }

    @Test
    public void updateTaskTest() {
        ToDoList todo = new ToDoList();

        todo.markStatusDone(1);
        String task = todo.getTaskById(1);

        assertEquals("1. Makan [DONE] Daily Activity", task);
    }

    @Test
    public void insertTaskTest() {
        ToDoList todo = new ToDoList();
        todo.insertTask(4, "Main", true, "Daily activity");
        String task = todo.getTaskById(4);
        assertEquals("4. Main [DONE] DAILY ACTIVITY", task);
        todo.insertTask(60, "Kuliah", true, "Daily activity");
        task = todo.getTaskById(60);
        assertEquals("60. Kuliah [DONE] DAILY ACTIVITY", task);
    }

    @Test
    public void removeTaskTest() {
        ToDoList todo = new ToDoList();
        todo.removeTask(2);
        assertNull(todo.getTaskById(2));
        todo.removeTask(3);
        assertNull(todo.getTaskById(3));
    }

    @Test
    public void searchByCategoryTest() {
        ToDoList todo = new ToDoList();
        assertEquals("1. Makan [DONE] Daily Activity\n" +
                        "2. Minum [NOT DONE] Daily Activity\n"

                , todo.searchByCategory("Daily Activity"));
    }


}