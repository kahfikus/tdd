package com.binus;

import com.binus.Model.ToDo;
import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoTest {

    @Test
    public void convertAsset() {
        ToDo task = new ToDo(1, "Do Dishes",true,"Daily Activity");
        ToDo expectedTask = new ToDo();
        expectedTask.setId(1);
        expectedTask.setName("Do Dishes");
        expectedTask.setStatus(true);
        assertEquals(expectedTask, task);
    }

    @Test
    public void todoIdIsSetAndGet(){
        ToDo task = new ToDo();
        int expectedId = 1;
        task.setId(expectedId);
        assertEquals(expectedId,task.getId());
    }


    @Test
    public void todoNameIsSetAndGet(){
        ToDo task = new ToDo();
        String expectedName = "Makan";
        task.setName(expectedName);
        assertEquals(expectedName,task.getName());
    }

    @Test
    public void taskStatusIsGetAndSet() {
        ToDo task = new ToDo();
        task.setStatus(true);
        assertTrue(task.getStatus());
    }



}