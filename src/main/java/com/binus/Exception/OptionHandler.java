package com.binus.Exception;

public class OptionHandler {
    private final int MIN_OPTION = 1;
    private final int MAX_OPTION = 6;

    public int isChoosable(int option) {
        if (option >= MIN_OPTION && option <= MAX_OPTION) return option;
        else return MAX_OPTION;
    }

}
