package com.binus.Model;


//Class Modifier


import java.util.Objects;

public class ToDo {

    private String name;
    private int  id ;
    private boolean status;
    private String category;


    public ToDo(){

    }

    public ToDo(int id, String name,  boolean status, String category) {

        this.name = name;
        this.id = id;
        this.status = status;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }


    public boolean getStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setStatusToDone() {
        this.status = true;
    }
    public String getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ToDo task = (ToDo) o;
        return id == task.id &&
                name.equals(task.name) &&
                status == task.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, status);
    }

    @Override
    public String toString() {
        String statusToString = this.status ? " [DONE]" : " [NOT DONE]";
        return this.id + ". " + this.name + statusToString + " " + this.category;
    }




}
