package com.binus.Main;

import com.binus.Exception.OptionHandler;
import com.binus.Repository.ToDoList;

import java.util.Scanner;

public class MainDisplay {

    private ToDoList todoList = new ToDoList();
    private Scanner in = new Scanner(System.in);
    private OptionHandler optionHandler = new OptionHandler();

    public void executable() {
        execute();
    }

    private void execute() {
        int choice;
        do {
            printMenu();
            choice = in.nextInt();
            in.nextLine();
            choice = optionHandler.isChoosable(choice);
            executeChosenOption(choice);
        } while (choice != 6);
    }

    private void executeChosenOption(int choice) {
        switch (choice) {
            case 1:
                System.out.println(todoList.getAllTask());
                break;
            case 2:
                updateMenu();
                break;
            case 3:
                insertMenu();
                break;
            case 4:
                deleteMenu();
                break;
            case 5:
                searchCategoryMenu();
                break;
        }
    }

    private void printMenu() {
        System.out.println("\nTodoList");
        System.out.println("=====================================");
        System.out.println("1. See all task");
        System.out.println("2. Update task");
        System.out.println("3. Input task");
        System.out.println("4. Delete task");
        System.out.println("5. Search by category");
        System.out.println("6. Exit\n");
    }

    private void updateMenu() {
        System.out.println(todoList.getAllTask());
        System.out.println("which task has been done?");
        int id = in.nextInt();
        System.out.println(todoList.markStatusDone(id));
    }

    private void deleteMenu() {
        System.out.println(todoList.getAllTask());
        System.out.println("which task will be deleted?");
        int id = in.nextInt();
        System.out.println(todoList.removeTask(id));
    }

    private void insertMenu() {
        int taskId = insertTaskId();
        String taskName = insertTaskName();
        boolean taskStatus = insertTaskStatus();
        String category = insertTaskCategory();
        todoList.insertTask(taskId, taskName, taskStatus, category);
    }

    private int insertTaskId() {
        System.out.println("Insert Task ID: ");
        int taskId;
        taskId = in.nextInt();
        in.nextLine();
        return taskId;
    }

    private String insertTaskName() {
        System.out.println("Insert task name");
        return in.nextLine();
    }

    private boolean insertTaskStatus() {
        boolean taskStatus = false;
        boolean loop;
        String temporary;
        do {
            loop = true;
            System.out.println("Insert task status (DONE / NOT DONE): ");
            temporary = in.nextLine();
            if ((temporary.equalsIgnoreCase("DONE"))) taskStatus = true;
            else if ((temporary.equalsIgnoreCase("NOT DONE"))) taskStatus = false;
            else loop = false;
        } while (loop == false);
        return taskStatus;
    }

    private String insertTaskCategory() {
        System.out.println("Insert task category: ");
        return in.nextLine();
    }

    private void searchCategoryMenu() {
        System.out.println("which category want to ?");
        String category = in.nextLine();
        System.out.println(todoList.searchByCategory(category));
    }

}
