package com.binus.Repository;

import com.binus.Model.ToDo;

import java.util.HashMap;
import java.util.Map;

public class ToDoList {
    private HashMap<Integer, ToDo> taskList;

    public ToDoList() {
        this.taskList = new HashMap<>();
        this.taskList.put(1, new ToDo(1,"Makan",true,"Daily Activity"));
        this.taskList.put(2, new ToDo(2,"Minum",false,"Daily Activity"));
        this.taskList.put(3, new ToDo(3,"Bermain Sepak Bola",true,"Olahraga"));
        this.taskList.put(4, new ToDo(4,"Bermain Basket",false,"Olahraga"));

    }

    public String getTaskById(int id) {
        return this.taskList.containsKey(id) ? this.taskList.get(id).toString() : null;
    }

    public String getAllTask() {
        StringBuilder tasks = new StringBuilder();
        for (HashMap.Entry<Integer, ToDo> task : this.taskList.entrySet()) {
            appendTaskToTaskList(tasks, task);
        }
        removeLastEscapeCharacter(tasks);
        return tasks.toString();
    }

    public String markStatusDone(int id) {
        ToDo task = this.taskList.get(id);
        if (task != null) {
            task.setStatusToDone();
            return task.toString();
        }
        return null;
    }

    public void insertTask(int taskId, String taskName, boolean taskStatus, String category) {
        this.taskList.put(taskId, new ToDo(taskId, taskName, taskStatus, category.toUpperCase()));
    }

    public String removeTask(int id) {
        this.taskList.remove(id);
        return getTaskById(id) == null ? "Delete Success" : "Delete Failed";
    }

    public String searchByCategory(String category) {
        StringBuilder tasks = new StringBuilder();
        for (HashMap.Entry<Integer, ToDo> task : this.taskList.entrySet()) {
            if (category.equalsIgnoreCase(task.getValue().getCategory())) {
                appendTaskToTaskList(tasks, task);
            }
        }
        if (tasks.length() != 0) {
            removeLastEscapeCharacter(tasks);
        } else return "No category found";
        return tasks.toString();
    }

    private void removeLastEscapeCharacter(StringBuilder tasks) {
        tasks.delete(tasks.length() - 1, tasks.length() + 1);
    }

    private void appendTaskToTaskList(StringBuilder tasks, Map.Entry<Integer, ToDo> task) {
        tasks.append(task.getValue().toString());
        tasks.append("\n");
    }




}
